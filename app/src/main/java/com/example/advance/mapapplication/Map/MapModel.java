package com.example.advance.mapapplication.Map;

import android.content.Context;

import com.example.advance.mapapplication.Map.ModelGoogleAddress.GoogleAddress;
import com.example.advance.mapapplication.Map.ModelGoogleAddress.Result;
import com.example.advance.mapapplication.Utilies.Constans;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MapModel implements MapContracts.Model {

    MapContracts.Presenter presenter = new MapPresenter();
    private Context mContext;

    @Override
    public void callGoogleAddressApi(String nameLocation) {

        final String[] lat = new String[1];
        final String[] lng = new String[1];

        Constans.googleAddressInterface.getLocation(nameLocation).enqueue(new Callback<GoogleAddress>() {
            @Override
            public void onResponse(Call<GoogleAddress> call, Response<GoogleAddress> response) {

               String status = response.body().getStatus();
               if(status.equals("OK")) {

                   for (Result result:
                           response.body().getResults()) {
                        lat[0] = result.getGeometry().getLocation().getLat().toString();
                        lng[0] = result.getGeometry().getLocation().getLng().toString();

                   }

                    presenter.getLocationApi(lat[0], lng[0]);

               } else {
                   presenter.getErrorCallApi("Error Call WebService, Check Your Connection");
               }
            }

            @Override
            public void onFailure(Call<GoogleAddress> call, Throwable t) {
                presenter.getErrorCallApi("Error Call WebService, Check Your Connection");
            }
        });
    }

    @Override
    public void attachContext(Context mContext) {

        this.mContext = mContext;
    }
}
