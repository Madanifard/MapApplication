package com.example.advance.mapapplication.Map;

import android.content.Context;

public interface MapContracts {

    interface View {

        void getErrorCallApi(String error);

        void getLocationApi(String lat, String lng);

    }

    interface Presenter {

        void attachView(View view);

        void attachContext(Context mContext);

        void getTextNameLocation(String nameLocation);

        void getErrorCallApi(String error);

        void getLocationApi(String lat, String lng);

    }

    interface Model {

        void attachContext(Context mContext);

        void callGoogleAddressApi(String nameLocation);
    }

}
