package com.example.advance.mapapplication.Utilies;


import com.example.advance.mapapplication.Map.ModelGoogleAddress.GoogleAddress;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface GoogleAddressInterface {

    @GET("/maps/api/geocode/json")
    Call<GoogleAddress> getLocation(@Query("address") String address);

}
