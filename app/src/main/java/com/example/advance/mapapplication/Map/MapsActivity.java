package com.example.advance.mapapplication.Map;

import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.advance.mapapplication.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.Map;

public class MapsActivity extends FragmentActivity implements MapContracts.View, OnMapReadyCallback, GoogleMap.OnCameraIdleListener, GoogleMap.OnCameraMoveStartedListener {

    private GoogleMap mMap;

    String lat, lng;
    String TAG = "map";
    EditText nameLocation;

    MapContracts.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);

        mapFragment.getMapAsync(this);

        initMvp();

        bindWidget();
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));

        setMapConfig();

    }

    /**
     * bind the widget
     */
    public void bindWidget() {

        nameLocation = findViewById(R.id.txt_name_location);
    }

    @Override
    public void getErrorCallApi(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void getLocationApi(String lat, String lng) {

//        LatLng sydney = new LatLng((float)lat, (float)lng);
//        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
    }

    /**
     * for Attach view to Presenter
     */
    public void initMvp() {

        presenter = new MapPresenter();
        presenter.attachView(this);
        presenter.attachContext(this);

    }

    /**
     * for get text when click on EditText
     * @param view
     */
    public void onClickSearchBtn(View view){

        Toast.makeText(this, nameLocation.getText().toString(), Toast.LENGTH_SHORT).show();

        presenter.getTextNameLocation(nameLocation.getText().toString());
    }

    /**
     * config for map
     */
    void setMapConfig() {

        mMap.setOnCameraIdleListener(this);
        mMap.setOnCameraMoveStartedListener(this);
        mMap.getUiSettings().setAllGesturesEnabled(true);
        mMap.getUiSettings().setCompassEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);
        mMap.getUiSettings().setTiltGesturesEnabled(true);
        mMap.getUiSettings().setZoomGesturesEnabled(true);
        mMap.getUiSettings().setScrollGesturesEnabled(true);
        mMap.getUiSettings().setMapToolbarEnabled(true);
        mMap.getUiSettings().setZoomControlsEnabled(true);

    }

    /**
     * get lat and log form map
     */
    @Override
    public void onCameraIdle() {

        lat = mMap.getCameraPosition().target.latitude + "";
        lng = mMap.getCameraPosition().target.longitude + "";
        Log.d(TAG, "onCameraIdle: lat: " + lat + ", lng: " + lng);
    }

    @Override
    public void onCameraMoveStarted(int i) {

    }

}
