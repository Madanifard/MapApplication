package com.example.advance.mapapplication.Map;

import android.content.Context;
import android.util.Log;

public class MapPresenter implements MapContracts.Presenter {



    private MapContracts.View view;
    private MapContracts.Model model;

    @Override
    public void getLocationApi(String lat, String lng) {
        view.getLocationApi(lat, lng);
    }

    @Override
    public void getTextNameLocation(String nameLocation) {
        model.callGoogleAddressApi(nameLocation);
    }

    @Override
    public void getErrorCallApi(String error) {
        view.getErrorCallApi(error);
    }

    @Override
    public void attachView(MapContracts.View view) {

        this.view = view;
        model = new MapModel();
    }

    @Override
    public void attachContext(Context mContext) {

        model.attachContext(mContext);
    }
}
